Node version: v8.9.2

Cordova version: 7.1.0

Config.xml file: 

<?xml version='1.0' encoding='utf-8'?>
<widget id="app.siasar.org" version="0.11.0" xmlns="http://www.w3.org/ns/widgets" xmlns:gap="http://phonegap.com/ns/1.0">
    <name>SIASApp</name>
    <description>SIASAR Mobile Application</description>
    <author email="admin@siasar.org" href="http://siasar.org">SIASAR</author>
    <content src="index.html" />
    <preference name="DisallowOverscroll" value="true" />
    <preference name="android-minSdkVersion" value="16" />
    <preference name="android-targetSdkVersion" value="27" />
    <icon src="icon.png" />
    <access origin="*" />
    <allow-intent href="http://*/*" />
    <allow-intent href="https://*/*" />
    <allow-intent href="tel:*" />
    <allow-intent href="sms:*" />
    <allow-intent href="mailto:*" />
    <allow-intent href="geo:*" />
    <platform name="android">
        <icon density="ldpi" src="www/res/icon/android/drawable-ldpi-icon.png" />
        <icon density="mdpi" src="www/res/icon/android/drawable-mdpi-icon.png" />
        <icon density="hdpi" src="www/res/icon/android/drawable-hdpi-icon.png" />
        <icon density="xhdpi" src="www/res/icon/android/drawable-xhdpi-icon.png" />
        <icon density="xxhdpi" src="www/res/icon/android/drawable-xxhdpi-icon.png" />
        <icon density="xxxhdpi" src="www/res/icon/android/drawable-xxxhdpi-icon.png" />
        <splash density="land-ldpi" src="www/res/screen/android/drawable-land-ldpi-screen.png" />
        <splash density="land-mdpi" src="www/res/screen/android/drawable-land-mdpi-screen.png" />
        <splash density="land-hdpi" src="www/res/screen/android/drawable-land-hdpi-screen.png" />
        <splash density="land-xhdpi" src="www/res/screen/android/drawable-land-xhdpi-screen.png" />
        <splash density="land-xxhdpi" src="www/res/screen/android/drawable-land-xxhdpi-screen.png" />
        <splash density="land-xxxhdpi" src="www/res/screen/android/drawable-land-xxxhdpi-screen.png" />
        <splash density="port-ldpi" src="www/res/screen/android/drawable-port-ldpi-screen.png" />
        <splash density="port-mdpi" src="www/res/screen/android/drawable-port-mdpi-screen.png" />
        <splash density="port-hdpi" src="www/res/screen/android/drawable-port-hdpi-screen.png" />
        <splash density="port-xhdpi" src="www/res/screen/android/drawable-port-xhdpi-screen.png" />
        <splash density="port-xxhdpi" src="www/res/screen/android/drawable-port-xxhdpi-screen.png" />
        <splash density="port-xxxhdpi" src="www/res/screen/android/drawable-port-xxxhdpi-screen.png" />
    </platform>
    <engine name="android" spec="7.0.0" />
    <plugin name="cordova-plugin-camera" spec="^2.4.1" />
    <plugin name="cordova-plugin-dialogs" spec="^1.3.4" />
    <plugin name="cordova-plugin-file" spec="^5.0.0" />
    <plugin name="cordova-plugin-geolocation" spec="^2.4.3" />
    <plugin name="cordova-plugin-globalization" spec="^1.0.8" />
    <plugin name="cordova-plugin-inappbrowser" spec="^1.7.2" />
    <plugin name="cordova-plugin-splashscreen" spec="^3.2.2" />
    <plugin name="cordova.plugins.diagnostic" spec="^3.7.2" />
    <plugin name="cordova-plugin-whitelist" spec="^1.3.3" />
    <plugin name="cordova-plugin-app-version" spec="^0.1.9" />
</widget>


Plugins: 

cordova-plugin-app-version,cordova-plugin-camera,cordova-plugin-compat,cordova-plugin-dialogs,cordova-plugin-file,cordova-plugin-geolocation,cordova-plugin-globalization,cordova-plugin-inappbrowser,cordova-plugin-splashscreen,cordova-plugin-whitelist,cordova.plugins.diagnostic

Android platform:

Available Android targets:
----------
id: 1 or "android-25"
     Name: Android 7.1.1
     Type: Platform
     API level: 25
     Revision: 3
     Skins: HVGA, QVGA, WQVGA400, WQVGA432, WSVGA, WVGA800 (default), WVGA854, WXGA720, WXGA800, WXGA800-7in
 Tag/ABIs : no ABIs.
----------
id: 2 or "android-26"
     Name: Android 8.0.0
     Type: Platform
     API level: 26
     Revision: 2
     Skins: HVGA, QVGA, WQVGA400, WQVGA432, WSVGA, WVGA800 (default), WVGA854, WXGA720, WXGA800, WXGA800-7in
 Tag/ABIs : no ABIs.



