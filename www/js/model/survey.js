/* global Store, Form, Session, Api, Relationship, i18next */

var Survey = function(data) {
  if (data) {
    this.id = data.id;
    this.formId = data.formId;
    this.finished = data.finished;
    this.uploaded = data.uploaded;
    this.name = data.name;
    this.serverId = data.serverId;
    this.form = data.form;
    this.data = data.data;
    this.updatedDate = data.updatedDate;
  }
};

Survey.DRAFT_STATUS = 2;
Survey.FINISHED_STATUS = 3;

Survey.list = function(params) {
  params = params || {};
  params.uploaded = false;
  return new Promise(function(resolve, reject) {
    Store.getInstance().getSurveys(params).then(function(surveys) {
      surveys = surveys.map(function(survey) {
        return Form.get(survey.formId).then(function(form) {
          survey = new Survey(survey);
          survey.form = form;
          return survey;
        });
      });
      Promise.all(surveys).then(function(results) {
        resolve(results);
      });
    }).catch(function(error) {
      reject(error);
    });
  });
};

Survey.get = function(id) {
  return new Promise(function(resolve, reject) {
    Store.getInstance().getSurvey(id).then(function(survey) {
      if (survey) {
        survey = new Survey(survey);
        Form.get(survey.formId).then(function(formId) {
          survey.form = formId;
          survey.form.parse(function(field) {
            field.value = survey.data[field.name];
          }, function(collection) {
            collection.value = survey.data[collection.name];
          }, function(field) {
            field.value = survey.data[field.collectionName][field.name];
          });
          resolve(survey);
        });
      } else {
        resolve(survey);
      }
    }).catch(function(error) {
      reject(error);
    });
  });
};

Survey.synchronize = function(forms, log) {
  return new Promise(function(resolve, reject) {
    var downloadPromises = forms.map(function(form) {
      return Survey.download(form, log);
    });
    Promise.all(downloadPromises).then(resolve).catch(reject);
  });
};

Survey.download = function(form, log) {
  return new Promise(function(resolve, reject) {
    var userId = Session.get().user.id;
    var downloadLog = log.add(i18next.t('download-surveys-message', { form: form.label }));
    var surveys = [];
    var imagePromises = [];

    Api.getSurveys(form.id, Survey.DRAFT_STATUS, userId).then(function(data) {
      $.each(data, function(key, draftedSurvey) {
        var survey = new Survey();
        survey.form = form;
        survey.serverId = draftedSurvey.entityform_id;
        survey.data = {};
        form.parse(function(field) {
          survey.data[field.name] = survey.getFieldValueFromApi(field, draftedSurvey[field.name.replace('field_', '')]);
          if (field.type === 'image_image' && survey.data[field.name] !== null) {
            imagePromises.push(Survey.downloadImage(survey, field));
          }
        }, function(collection) {
          survey.data[collection.name] = [];
          $.each(draftedSurvey[collection.name.replace('field_', '')], function(index, collectionItem) {
            survey.data[collection.name].push({
              id: collectionItem.id
            });
          });
        }, function(field) {
          $.each(draftedSurvey[field.collectionName.replace('field_', '')], function(index) {
            survey.data[field.collectionName][index][field.name] =
              survey.getFieldValueFromApi(field,
                draftedSurvey[field.collectionName.replace('field_', '')][index][field.name]);
            if (field.type === 'image_image' && survey.data[field.name] !== null) {
              imagePromises.push(Survey.downloadImage(survey.data[field.name]));
            }
          });
        });
        survey.prepareSave();
        surveys.push(survey);
        log.remove(downloadLog);
      });

      Promise.all(imagePromises).then(function() {
        var surveyPromises = surveys.map(function(survey) {
          return survey.save();
        });
        Promise.all(surveyPromises).then(resolve).catch(reject);
      }).catch(reject);
    }).catch(reject);
  });
};

Survey.downloadImage = function(survey, field) {
  return new Promise(function(resolve, reject) {
    Api.getFile(survey.data[field.name]).then(function(fileData) {
      survey.data[field.name] = 'data:image/jpeg;base64,' + fileData.file;
      resolve();
    }).catch(reject);
  });
};

Survey.prototype.prepareSave = function(finished, uploaded) {
  this.finished = finished || false;
  this.uploaded = uploaded || false;
  this.formId = this.form.id;
  // TODO: the name should be set to the value of the field marked as identifier
  this.name = this.data.field_entity_name || this.form.label;
};

Survey.prototype.save = function(finish) {
  var self = this;
  this.prepareSave(finish === true, false);

  function saveRelations(field) {
    if (field.resource !== 'entityform') {
      return;
    }
    if (field.collectionName) {
      self.data[field.collectionName].forEach(function(item, index) {
        if (parseInt(item[field.name], 10) < 0) {
          new Relationship({
            fieldName: field.name,
            childId: self.id,
            collectionName: field.collectionName,
            index: index,
            parentId: item[field.name]
          }).save();
        }
      });
    } else if (parseInt(self.data[field.name], 10) < 0) {
      new Relationship({
        fieldName: field.name,
        childId: self.id,
        parentId: self.data[field.name]
      }).save();
    }
  }
  return Store.getInstance().saveSurvey(this).then(function() {
    self.form.parse(saveRelations, null, saveRelations);
  });
};

Survey.prototype.finish = function() {
  this.prepareSave();
  this.finished = true;
  return Store.getInstance().saveSurvey(this).then(this.save(true));
};

Survey.prototype.delete = function() {
  return Store.getInstance().deleteSurvey(this);
};

Survey.prototype.send = function() {
  var self = this;

  return new Promise(function(resolve, reject) {
    Session.logout().then(function() {
      Session.create(true).then(function(session) {
        var date = Math.round(Date.now() / 1000);
        var data = {
          type: self.formId,
          user: session.user.id,
          created: date,
          status: Survey.FINISHED_STATUS
        };
        var imagePromises = [];
        self.form.parse(function(field) {
          if (field.type === 'image_image' && self.data[field.name] !== null) {
            imagePromises.push(self.sendImage(field));
          }
        });
        Promise.all(imagePromises).then(function() {
          self.form.parse(function(field) {
            data[field.name] = self.getFieldValue(field, self.data[field.name]);
          }, null, function() {});
          Api.sendSurvey(data, self.id).then(function(result) {
            var oldId = self.id;
            var chain = Store.getInstance().updateId(self, result.entityform_id);
            self.form.parse(null, function(collection) {
              $.each(self.data[collection.name], function(index, collectionData) {
                chain = chain.then(function() {
                  return self.sendCollection(collection, collectionData);
                });
              });
            });
            chain.then(function() {
              self.prepareSave(true, true);
              Store.getInstance().saveSurvey(self).then(function() {
                var children = Relationship.getChildren(oldId);
                var surveyPromises = children.map(function(relation) {
                  return Survey.get(relation.childId);
                });
                Promise.all(surveyPromises).then(function(surveys) {
                  var savePromises = [];
                  surveys.forEach(function(survey, index) {
                    var child = children[index];
                    if (child.collectionName) {
                      child.indexes.forEach(function(item) {
                        survey.data[child.collectionName][item][child.fieldName] = self.id;
                      });
                    } else {
                      survey.data[child.fieldName] = self.id;
                    }
                    if (survey.finished) {
                      savePromises.push(survey.save(true));
                    } else {
                      savePromises.push(survey.save());
                    }
                  });
                  Relationship.deleteChildren(oldId);
                  Promise.all(savePromises).then(function() {
                    resolve(self);
                  });
                })
                ;
              });
            });
          }).catch(function(error) {
            reject(error);
          });
        }).catch(function(error) {
          reject(error);
        });
      }).catch(function(error) {
        app.error(error);
        $.mobile.loading('hide');
      });
    });
  });
};

Survey.prototype.sendImage = function(field) {
  var self = this;
  return new Promise(function(resolve, reject) {
    Api.sendFile({
      file: self.data[field.name].replace('data:image/jpeg;base64,', ''),
      uid: Session.get().user.id,
      filename: field.name + '_' + Date.now() + '.jpg'
    }).then(function(result) {
      field.fid = result.fid;
      resolve();
    }).catch(function(error) {
      reject(error);
    });
  });
};

Survey.prototype.sendCollection = function(collection, data) {
  var self = this;
  return new Promise(function(resolve, reject) {
    var collectionData = {
      name: collection.name,
      host_entity: {
        id: self.id,
        resource: 'entityform'
      }
    };

    self.form.parse(null, function(obj) {
      if (obj.id === collection.name) {
        $.each(collection.collection, function(index, value) {
          if (!value.hasOwnProperty('children')) {
            collectionData[value.name] = self.getFieldValue(value, data[value.name]);
          } else {
            collectionData = self.parseChild(collectionData, value, data);
          }
        });
      }
    }, null);

    Api.sendFieldCollection(collectionData, data['id']).then(function(result) {
      resolve(result);
    }).catch(function(error) {
      reject(error);
    });
  });
};

Survey.prototype.parseChild = function (collectionData, child, data) {
  var self = this;
  if (child.hasOwnProperty('children')) {
    return this.parseChild(collectionData, child.children, data);
  }
  $.each(child, function(index, value) {
    if (value.hasOwnProperty('children')) {
      return self.parseChild(collectionData, value.children, data);
    }
    collectionData[value.name] = self.getFieldValue(value, data[value.name]);
    return collectionData;
  });
  return collectionData;
};

Survey.prototype.getFieldValueFromApi = function(field, value) {
  var fieldValue = null;

  if (typeof value === 'undefined' || value === null) {
    return fieldValue;
  }

  // FIXME: this shouldn't be hardcoded
  if (field.name === 'field_pais') {
    return value.id;
  }

  function getDateSelectValue() {
    fieldValue = Math.round(Date.parse(value) / 1000);
  }

  function getNumberOrYear() {
    if (field.module === 'calendar_year') {
      value = value.toString().concat('-02-02');
      getDateSelectValue();
    } else {
      fieldValue = Number(value);
    }
  }

  function getOptionButtonsValue() {
    if (!field.multiple) {
      fieldValue = (value === true) ? 1 : 0;
    } else {
      fieldValue = [];
      value.forEach(function(item, index) {
        fieldValue[index] = item.tid;
      });
    }
    return fieldValue;
  }

  function getImageValue() {
    if (value.file) {
      fieldValue = value.file.id;
    }
  }

  function getMVFValue() {
    fieldValue = {
      value: value.value,
      target_id: value.unit
    };
  }

  function getGeoFieldValue() {
    if (value.lat !== null && value.lon !== null) {
      fieldValue = {
        latitude: value.lat,
        longitude: value.lon
      };
    }
  }

  function getOptionSelect() {
    switch (field.resource) {
      case 'taxonomy_term':
        fieldValue = value.tid;
        break;
      case 'user':
        fieldValue = value.uid;
        break;
      default:
        fieldValue = value;
    }
  }

  switch (field.type) {
    case 'geofield_latlon':
      getGeoFieldValue();
      break;
    case 'date_select':
      getDateSelectValue();
      break;
    case 'mvf_widget_default':
      getMVFValue();
      break;
    case 'image_image':
      getImageValue();
      break;
    case 'options_buttons':
      getOptionButtonsValue();
      break;
    case 'siasar_hierarchical_select':
      fieldValue = {
        label: value.name,
        value: value.tid
      };
      break;
    case 'number':
      getNumberOrYear();
      break;
    case 'options_select':
      getOptionSelect();
      break;
    default:
      fieldValue = value;
  }

  return fieldValue;
};

Survey.prototype.getFieldValue = function(field, value) {
  var fieldValue = null;
  if (typeof value === 'undefined' || value === null) {
    return fieldValue;
  }

  function getDateSelectValue() {
    fieldValue = Math.round(Date.parse(value) / 1000);
  }

  function getNumberOrYear() {
    if (field.module === 'calendar_year') {
      value = value.toString().concat('-02-02');
      getDateSelectValue();
    } else {
      fieldValue = value;
    }
  }

  function getOptionButtonsValue() {
    if (!Array.isArray(value) || value.length) {
      fieldValue = value;
    }
  }

  function getImageValue() {
    if (field.fid !== null) {
      fieldValue = {
        fid: field.fid
      };
    }
  }

  function getMVFValue() {
    fieldValue = {
      value: value.value,
      target_id: value.unit
    };
  }

  function getGeoFieldValue() {
    if (value.latitude !== null && value.longitude !== null) {
      fieldValue = {
        lat: value.latitude,
        lon: value.longitude
      };
    }
  }

  switch (field.type) {
    case 'geofield_latlon':
      getGeoFieldValue();
      break;
    case 'date_select':
      getDateSelectValue();
      break;
    case 'mvf_widget_default':
      getMVFValue();
      break;
    case 'image_image':
      getImageValue();
      break;
    case 'options_buttons':
      getOptionButtonsValue();
      break;
    case 'siasar_hierarchical_select':
      fieldValue = value.value;
      break;
    case 'number':
      getNumberOrYear();
      break;
    default:
      fieldValue = value;
  }

  return fieldValue;
};
