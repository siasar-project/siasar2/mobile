/* global User, i18next */
var HomeView = function() {
};

HomeView.prototype.show = function() {
  app.closeLeftPanel();

  function loadPage(data) {
    data = data || {};
    data.user = User.get();
    $('#home .session-info').html(Handlebars.templates['home'](data));
    $('#home .session-info a[data-function="show-config"]').click(function() {
      app.config.show();
    });
    $.mobile.navigate('#home');
    app.setHeader(i18next.t('app-name'));
  }

  if (cordova.getAppVersion) {
    cordova.getAppVersion.getVersionNumber(function(version) {
      loadPage({ version: version });
    }, function() {
      loadPage();
    });
  } else {
    loadPage();
  }
};
